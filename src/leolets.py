from pathlib import Path
from functools import wraps


def log(file_name):
	def add_logs(fn):
		@wraps(fn)
		def write_to_logs():
			path = Path.cwd() / f'logs/{file_name}'
			path.parent.mkdir(exist_ok=True)
			path.touch()
			with path.open(mode='a') as file:
				file.write(f'''
					Logging from: {fn.__name__},
					This is a mock log\n''')
			print(f'Logging from: {fn.__name__}')
			return fn()
		return write_to_logs
	return add_logs


@log('leolet.log')
def leolet():
	return f'This is {leolet.__name__} function'


if __name__ == '__main__':
	print(leolet())